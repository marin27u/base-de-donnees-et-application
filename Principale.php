<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 20/03/2017
 * Time: 10:27
 */

require 'vendor/autoload.php';
require_once 'vendor/fzaninotto/faker/src/autoload.php';

use bdd\model\Utilisateur;
use bdd\model\Commentaire;

$faker = Faker\Factory::create('fr_FR');

$db = new \Illuminate\Database\Capsule\Manager();

$chemin = [
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'gamepedia',
    'username' => 'game',
    'password' => 'game',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
];

$db->addConnection($chemin);
$db->setAsGlobal();
$db->bootEloquent();
$db::connection()->enableQueryLog();

/*
// usr
for ($i=0; $i<25000; $i++){
$usr=new Utilisateur();
$usr->nom=$faker->name;
$usr->prenom=$faker->firstName;
$usr->email=$faker->email;
$usr->tel=$faker->phoneNumber;
$usr->date=$faker->dateTime;

$usr->save();
}
*/
// comments

for ($i=0;$i<250000;$i++){
    $com=new Commentaire();
    $com->titre=$faker->title;
    $com->contenu=$faker->word;
    $com->date=$faker->dateTime;
    $com->created_at=$faker->dateTime;
    $com->update_at=$faker->dateTime;
    $com->save();
}

echo "c'est bon ma gueul";