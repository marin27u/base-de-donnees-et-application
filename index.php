<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 27/02/2017
 * Time: 10:29
 */
require 'vendor/autoload.php';

use \bdd\Controleur\ControleurAccueil;
use \bdd\Controleur\Controleur2;
use \bdd\Controleur\Controleur3;
use \bdd\Controleur\ControleurApi;

$db = new \Illuminate\Database\Capsule\Manager();

$chemin = [
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'gamepedia',
    'username' => 'game',
    'password' => 'game',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
];

$db->addConnection($chemin);
$db->setAsGlobal();
$db->bootEloquent();
$db::connection()->enableQueryLog();

$app =new \Slim\Slim();


$app->get('/',function (){(new ControleurAccueil())->accueil();})->name('Accueil');

$app->get('/q1',function (){(new ControleurAccueil())->q1();})->name('q1');

$app->get('/q2',function (){(new ControleurAccueil())->q2();})->name('q2');

$app->get('/q3',function (){(new ControleurAccueil())->q3();})->name('q3');

$app->get('/q4',function (){(new ControleurAccueil())->q4();})->name('q4');

$app->get('/q5',function (){(new ControleurAccueil())->q5();})->name('q5');

$app->get('/q5/:id',function ($id){(new ControleurAccueil())->q5b($id);})->name('id');

$app->get('/seance2',function (){(new Controleur2())->q1();})->name('seance');

$app->get('/seance22',function (){(new Controleur2())->q2();})->name('seance2');

$app->get('/seance3',function (){(new Controleur3())->q1();})->name('sean3');

$app->get('/api/games/:id',function ($id){(new ControleurApi())->id($id);})->name('apiId');

$app->get('/api/games',function (){(new ControleurApi())->jeu();})->name('apiJeu');

$app->run();