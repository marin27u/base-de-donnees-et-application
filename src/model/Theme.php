<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 27/02/2017
 * Time: 10:33
 */


namespace bdd\model;

class Theme extends  \Illuminate\Database\Eloquent\Model{

    protected $table = 'theme';
    protected $primaryKey = 'id';
    public $timestamps = false;


    function Game(){

        return $this->belongsToMany('bdd\game\Game','game2theme','game_id','theme_id');
    }


}