<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 27/02/2017
 * Time: 10:33
 */

namespace bdd\model;


class Game extends  \Illuminate\Database\Eloquent\Model{

    protected $table = 'game';
    protected $primaryKey = 'id';
    public $timestamps = false;


    function developpers(){
        return $this->belongsToMany('bdd\model\Company', 'game_developers','game_id', 'comp_id');

    }


    function publishers(){
        return $this->belongsToMany('bdd\model\Company', 'game_publishers','game_id', 'comp_id');
    }

    function plateform(){
        return $this->belongsTo('bdd\model\Platform', 'p_id');

    }

    function game2theme(){
        return $this->belongsToMany('bdd\model\Theme', 'game2theme','game_id', 'theme_id');
    }

    function characterFirst(){
        return $this->hasMany('bdd\model\Charactere', 'first_appear_in_game_id');

    }

    function characters(){
        return $this->belongsToMany('bdd\model\Charactere', 'game2character', 'game_id', 'character_id');

    }
    function gameRating(){

        return $this->belongsToMany('bdd\model\Rating', 'game2rating','game_id', 'rating_id');
    }

    function game2genre(){
        return $this->belongsToMany('bdd\model\Genre', 'game2genre', 'game_id', 'genre_id');
    }

    function similarGame(){
        return $this->belongsToMany('bdd\model\Game', 'similar_games', 'game1_id', 'game2_id');

    }












}