<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 06/03/2017
 * Time: 09:44
 */

namespace bdd\model;


class Rating extends  \Illuminate\Database\Eloquent\Model{

    protected $table = 'game_rating';
    protected $primaryKey = 'id';
    public $timestamps = false;


    function Game(){

        return $this->belongsToMany('bdd\game\Game','game2rating','rating_id','game_id');
    }

    function Board(){


        return $this->belongsTo('bdd\game\Board','b_id');
    }

}

