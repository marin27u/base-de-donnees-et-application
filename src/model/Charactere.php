<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 27/02/2017
 * Time: 10:33
 */

namespace bdd\model;

class Charactere extends  \Illuminate\Database\Eloquent\Model{

    protected $table = 'character';
    protected $primaryKey = 'id';
    public $timestamps = false;



    function gameFirst(){
        return $this->belongsTo('bdd\model\Game','first_appear_in_game_id');

    }



    function games(){

        return $this->belongsToMany('bdd\model\Game','game2character', 'character_id','game_id');
    }

    function enemi(){
        return $this->belongToMany('bdd\model\Charactere', 'enemis','char1_id', 'char2_id');
    }

    function friends(){

        return $this->belongToMany('bdd\model\Charactere', 'friends','char1_id', 'char2_id');
    }
}