<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 06/03/2017
 * Time: 09:48
 */

namespace bdd\model;

class Board extends  \Illuminate\Database\Eloquent\Model{

    protected $table = 'rating_board';
    protected $primaryKey = 'id';
    public $timestamps = false;


    function Rating(){

        return $this->hasMany('bdd\game\Rating','r_id');
    }

}

