<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 20/03/2017
 * Time: 08:58
 */

namespace bdd\model;

class Utilisateur extends  \Illuminate\Database\Eloquent\Model{


    protected $table = 'utilisateur';
    protected $primaryKey = 'email';
    public $timestamps = false;

	
	function aPoster(){
		     return $this->belongsToMany('bdd\model\Commentaire', 'aPoster','utilisateur_email', 'commentaire_titre');
		
	}
}