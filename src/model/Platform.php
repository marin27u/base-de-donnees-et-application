<?php

namespace bdd\model;


class Platform extends  \Illuminate\Database\Eloquent\Model{

    protected $table = 'platform';
    protected $primaryKey = 'id';
    public $timestamps = false;


    function Producer(){
        return $this->belongsTo('bdd\game\Company','c_id');
    }

    function Game(){

        return $this->hasMany('bdd\game\Game','p_id');
    }
}