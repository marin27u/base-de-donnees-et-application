<?php

namespace bdd\model;

class Commentaire extends  \Illuminate\Database\Eloquent\Model{


    protected $table = 'commentaire';
    protected $primaryKey = 'titre';
    public $timestamps = false;
}