<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 27/02/2017
 * Time: 10:33
 */

namespace bdd\model;

class Company extends  \Illuminate\Database\Eloquent\Model{

    protected $table = 'company';
    protected $primaryKey = 'id';
    public $timestamps = false;


    function Platform(){

        return $this->hasMany('bdd\model\Platform','c_id');
    }


    function developpers(){

        return $this->belongsToMany('bdd\model\Game','game_developers','comp_id','game_id');
    }

    function publisher(){

        return $this->belongsToMany('bdd\model\Game','game_publishers','comp_id','game_id');
    }
}