<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 06/03/2017
 * Time: 09:55
 */
namespace bdd\model;


class Genre extends  \Illuminate\Database\Eloquent\Model{

    protected $table = 'genre';
    protected $primaryKey = 'id';
    public $timestamps = false;

    function Game(){

        return $this->belongsToMany('bdd\game\Game','game2genre','genre_id','game_id');
    }
}