<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 06/03/2017
 * Time: 10:15
 */

namespace bdd\Controleur;

use bdd\model\Charactere;
use \bdd\model\Game;
use \bdd\model\Platform;
use \bdd\model\Company;
use Slim\Slim;



class Controleur2{


    function q1()
    {

        $id = Game::find('12342');

        echo("<h1>Requête 1 : </h1> <br>");

        $char = $id->characters()->get();

        foreach ($char as $game) {

            echo('Le nom est :' . $game->name . ' ||  Le deck est :  ' . $game->deck . '<br><br>');
        }

        echo("<br><br><br>");


        echo("<h1>Requête 2 : </h1><br>");


        $req = Game::where('name', 'like', 'Mario%')->get();

        foreach ($req as $game) {

            $tmp = $game->characters()->get();

            foreach ($tmp as $perso) {

                echo($perso->name . " <br><br>");
            }
        }


        echo("<br><br><br>");


        echo("<h1>Requête 3 : </h1><br>");


        $req = Company::where('name', 'like', '%Sony%')->get();

        foreach ($req as $game) {

            $tmp = $game->developpers()->get();

            foreach ($tmp as $jeu) {

                echo($jeu->name . " <br><br>");
            }
        }

        echo("<br><br><br>");


        echo("<h1>Requête 4: </h1><br>");


        $req = Game::where('name', 'like', '%Mario%')->get();

        foreach ($req as $game) {

            $tmp = $game->gameRating()->get();

            foreach ($tmp as $jeu) {

                echo($jeu->name . " <br><br>");
            }
        }
    }

    function q2(){


        echo ("<h1>Requête 5: </h1><br>");


        $req =Game::where('name','like','Mario%')->get();

        foreach ($req as $game){

            $tmp = $game->characters()->get();

            if(count($tmp) > 3){

                foreach ($tmp as $jeu){

                    echo ($game->name." <br><br>");
                }
            }
        }

        echo ("<br><br><br>");


        echo ("<h1>Requête 6: </h1><br>");


        $req =Game::where('name','like','Mario%')->get();

        foreach ($req as $game){

            $tmp = $game->gameRating()->where('name','like','%3+%')->get();

            foreach ($tmp as $jeu){

                echo ($game->name." <br><br>");
            }
        }

        echo ("<br><br><br>");

        echo ("<h1>Requête 7: </h1><br>");


        $req =Company::where('name','like','%Inc.%')->get();

        foreach ($req as $game){

            $comp = $game->publisher()->where('name','like','Mario%')->get();

            foreach ($comp as $company) {

                $rating = $company->gameRating()->where('name','like','%3+%')->get();

                foreach ($rating as $jeu)

                echo($company->name . "<br><br>");
            }

            }
        echo ("<br><br><br>");

        echo ("<h1>Requête 8: </h1><br>");


        $req =Company::where('name','like','%Inc.%')->get();

        foreach ($req as $game){

            $comp = $game->publisher()->where('name','like','Mario%')->get();

            foreach ($comp as $company) {

                $rating = $company->gameRating()->where('name','like','%3+%','and','name','like','%CERO%')->get();

                foreach ($rating as $jeu)

                    echo($company->name . "<br><br>");
            }

        }

    }



}