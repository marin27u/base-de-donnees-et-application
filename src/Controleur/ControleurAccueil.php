<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 05/03/2017
 * Time: 09:55
 */
namespace bdd\Controleur;


use \bdd\model\Game;
use \bdd\model\Platform;
use \bdd\model\Company;
use Slim\Slim;


class ControleurAccueil {

    function __construct()
    {

    }


    function accueil(){

        $app = Slim::getInstance();

        $url1 =$app->urlFor('q1');
        $url2 = $app-> urlFor('q2');
        $url3 = $app-> urlFor('q3');
        $url4 = $app->urlFor('q4');
        $url5 = $app->urlFor('q5');
        $url6 = $app->urlFor('seance');
        $url7=$app->urlFor('sean3');
        $url8=$app->urlFor('seance2');

        $html = <<<END
        
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BDD</title>
</head>
<body>

<h1> Question 1 </h1> <a href="$url1" > Ici </a>
<br>
<br>

<h1> Question 2 </h1> <a href="$url2" > Ici </a>
<br>
<br>

<h1> Question 3 </h1> <a href="$url3" > Ici </a>
<br>
<br>

<h1> Question 4 </h1> <a href="$url4" > Ici </a>
<br>
<br>

<h1> Question 5 </h1> <a href="$url5" > Ici </a>
<br>
<br>

<h1>Seance 2, Question 1 à 4 </h1> <a href="$url6" > Ici </a>
<br>
<br>
<h1>Seance 2, Question 5 à 8 </h1> <a href="$url8" > Ici </a>
<br>
<br>
<h1>Seance 3 </h1> <a href="$url7" > Ici </a>
<br>
<br>

</body>
</html>


END;

        echo $html;


    }



    function q1(){
        $req = Game::where('name','like','%Mario%')->get();

        echo ("<h1>Question 1 : </h1> <br>");

        foreach ($req as $games){

            echo ("L'id : ".$games->id. "  Le jeu : ");
            echo ($games->name."<br>");
        }

        echo ("<br><br><br>");

    }

    function q2(){

        $req = Company::where('location_country','=','Japan')->get();


        echo ("<h1>Question 2 : </h1> <br>");

        foreach ($req as $games){

            echo ("Le nom : ".$games->name. "<br> ");
        }


        echo ("<br><br><br>");

    }

    function q3(){

        $req =Platform::where('install_base','>=','10000000')->get();


        echo ("<h1>Question 3 : </h1> <br>");

        foreach ($req as $games){

            echo ("Le nom : ".$games->name. "<br> ");
        }

        echo ("<br><br><br>");

    }

    function q4(){

        $req =Game::where('id','>=','21173')->get();


        echo ("<h1>Question 4 : </h1> <br>");

        $i = 0;
        foreach ($req as $games){

            if($i <442) {
                echo("Le nom : " . $games->name . "<br> ");
                $i++;
            }
            else{
                break;
            }
        }

        echo $i;
    }



    function q5(){

        $req = Game::where('id','<=','500')->get();

        $id =500;

        $app =Slim::getInstance();

        $url = $app->urlFor('id',['id' => $id]);

        echo ("<h1>Question 5 : </h1> <br>");

        $taille = count($req);

        echo ("<a href=$url>Les 500 suivants</a><p>     Le nombre de résultat : $taille</p><br><br>");

        foreach ($req as $games){

            echo ("Le nom : ".$games->name. " ||  Le deck :  ".$games->deck."<br><br><br>");
        }

    }


    function q5b($id){

        $req = Game::whereBetween('id',array($id,$id+499))->get();

        $app =Slim::getInstance();

        $url = $app->urlFor('id',['id' => $id+500]);

        echo ("<h1>Question 5 : </h1> <br>");

        $taille = count($req);

        echo ("<a href=$url>Les 500 suivants</a><p>     Le nombre de résultat : $taille</p><br><br>");

        foreach ($req as $games){

            echo ("Le nom : ".$games->name. " ||  Le deck :  ".$games->deck."<br><br><br>");
        }
    }



}