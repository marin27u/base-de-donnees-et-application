<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 28/03/2017
 * Time: 08:19
 */

namespace bdd\Controleur;


use bdd\model\Game;
use Slim\Slim;

class ControleurApi{



    function id($id){

        $query = Game::where('id','=',$id)->first();

        $app =Slim::getInstance();

        $data =array("id"=>$query->id,"name"=>$query->name,

            "deck"=>$query->deck,"description"=>$query->description,

            "original_release_date"=>$query->original_release_date);


        $reponse = $app->response();

        $reponse['Content-Type']='application/json';
        $reponse->status(200);

        $reponse->body(json_encode($data));

        //echo json_encode($data);



    }


    function jeu()
    {

        $app = Slim::getInstance();

        $paramValue = $app->request()->get('page');

        $indice = "";

        if ($paramValue != 0){

            $debut =$paramValue*200 -200;
            $fin =$paramValue*200;

            $query = Game::whereBetween('id',array($debut,$fin))->get();

            foreach ($query as $item) {

                if ($indice <= 200) {

                    $reponse = $app->response();

                    $reponse['Content-Type'] = 'application/json';
                    $reponse['charset'] = 'utf-8';
                    $reponse->status(200);

                    $data['games'] = array("id" => $item->id, "name" => $item->name,

                        "deck" => $item->deck, "description" => $item->description,

                        "original_release_date" => $item->original_release_date, ",");

                    $data['links'] =array("self"=> "{ \"href\" : \"/api/games/$item->id");

                    echo json_encode($data,JSON_PRETTY_PRINT);


                }
            }
            $prev=$paramValue-1;
            $next=$paramValue+1;

            $data['links']=array("prev"=>"{ \"href\" : \"/api/games?page=$prev","next"=>"{ \"href\" : \"/api/games?page=$next");

            echo json_encode($data,JSON_PRETTY_PRINT);
        }

        else {


            $query = Game::where('id', '<', '201')->get();

            foreach ($query as $item) {

                if ($indice <= 200) {

                    $reponse = $app->response();

                    $reponse['Content-Type'] = 'application/json';
                    $reponse->status(200);

                    $data['games'] = array("id" => $item->id, "name" => $item->name,

                        "deck" => $item->deck, "description" => $item->description,

                        "original_release_date" => $item->original_release_date, ",");

                    $data['links'] =array("self"=> "{ \"href\" : \"/api/games/$item->id");

                    echo json_encode($data,JSON_PRETTY_PRINT);

                }

            }


        }
    }
}