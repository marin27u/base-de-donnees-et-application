<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 13/03/2017
 * Time: 09:17
 */

namespace bdd\Controleur;

use bdd\model\Charactere;
use bdd\model\Game;

class Controleur3 {


    function q1(){
        echo('<h1> Question 1 :</h1>');
        $t1 = microtime(true);
        $req = Game::get();
        $t2 = microtime(true) - $t1;

        echo('Le temps d execution est :'.$t2."<br><br><br>");


        echo('<h1> Question 2 :</h1>');
        $t1 = microtime(true);
        $req =Game::where('name','like','Mario%')->get();
        $t2 = microtime(true) - $t1;

        echo('Le temps d execution est :'.$t2."<br><br><br>");

        echo('<h1> Question 3 :</h1>');
        $t1 = microtime(true);

        echo('Le temps d execution est :'.$t2."<br><br><br>");


        echo('<h1> Question 4 :</h1>');
        $t1 = microtime(true);
        $req =Game::where('name','like','Mario%')->whereHas('gameRating',function ($q){

            $q->where('name','like','%3+%');
        })->get();
        $t2 = microtime(true) - $t1;

        echo('Le temps d execution est :'.$t2."<br><br><br>");

    }
}